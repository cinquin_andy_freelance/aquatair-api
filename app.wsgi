#!/usr/bin/python3

import logging
import sys
logging.basicConfig(stream=sys.stderr)
sys.path.insert(0, '/home/apiuser/aquatair-api/')
sys.path.append('/home/apiuser/aquatair-api/venv/lib/python3.7/site-packages/')

def application(environ, start_response):
    status = '200 OK'
    output = b'Hello World! \n'
    response_headers = [('Content-type', 'text/plain'),
               ('Content-Length', str(len(output)))]


    start_response(status, response_headers)
    return [output]


from app import app as application
application.secret_key = b'sdqfjqsdfqizroqnxw0c'
