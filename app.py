# !/usr/bin/env python3
#
# This application demonstrates how access control can be implemented for
# flask-restful API endpoints
# see also https://flask-restful.readthedocs.io/en/latest/extending.html#resource-method-decorators
#
import sys
from safrs import SAFRSBase, SAFRSAPI, jsonapi_rpc
from flask_sqlalchemy import SQLAlchemy
from flask_httpauth import HTTPBasicAuth
from flask_cors import CORS
from flask import Flask
from uuid import uuid1
import bcrypt

db = SQLAlchemy()

# Authentication with flask-httpauth
# https://flask-httpauth.readthedocs.io/en/latest/
auth = HTTPBasicAuth()


@auth.verify_password
def verify_password(username, token):
    # Implement your authentication here
    user = User.query.filter_by(username=username).first()
    if user.token == token:
        return True
    return False


class User(SAFRSBase, db.Model):
    """
        description: Authentification User for app security
    """
    __tablename__ = "user"
    id = db.Column(db.String(255), primary_key=True)
    username = db.Column(db.String(50))
    password = db.Column(db.String(255))
    token = db.Column(db.String(255))

    @classmethod
    @jsonapi_rpc(http_methods=['GET'])
    def auth(self, **args):
        '''

        description : Send an email
        args:
            username: test email
            password: test email
        parameters:
            - name : username
            - name : password
        '''

        user = User.query.filter_by(username=args['username']).first()
        if user is None:
            return {'error': 'Erreur : l\'utilisateur n\'existe pas'}
        if bcrypt.checkpw(args['password'].encode(), user.password.encode()):
            user.token = uuid1().hex
            db.session.commit()
            # print(bcrypt.hashpw(user.password.encode(), bcrypt.gensalt()))
            return {'token': '{}'.format(user.token)}
        return {'error': 'Erreur : le mot de passe est invalide'}

    @classmethod
    @jsonapi_rpc(http_methods=['GET'])
    def checkToken(self, **args):
        '''

        description : check token
        args:
            token: string
        parameters:
            - name : token
        '''

        user = User.query.filter_by(username=args['username']).first()
        if user is None:
            return {'error': 'authentification : user dont exist'}
        if user.token == args['token']:
            return {'ok'}
        return {'bad'}


class Category(SAFRSBase, db.Model):
    """
        description: Category for Gears
    """
    __tablename__ = "category"
    id = db.Column(db.String(255), primary_key=True)
    shortId = db.Column(db.String(32))
    name = db.Column(db.String(50))
    description = db.Column(db.String(500))
    gears = db.relationship('Gear', cascade="all,delete", backref='category', lazy=False)


class Gear(SAFRSBase, db.Model):
    """
        description: Gears of the company
    """

    __tablename__ = "gear"
    id = db.Column(db.String(255), primary_key=True)
    shortId = db.Column(db.String(32))

    name = db.Column(db.String(50))
    gearType = db.Column(db.String(50))
    mark = db.Column(db.String(50))
    usury = db.Column(db.String(50))

    purchaseDate = db.Column(db.DateTime)
    endLifeDate = db.Column(db.DateTime)

    description = db.Column(db.String(500))
    categoryId = db.Column(db.String(255), db.ForeignKey('category.id', ondelete='CASCADE'), nullable=False)

    affectations = db.relationship('Affectation',cascade="all,delete", backref='gear', lazy=False)


class Affectation(SAFRSBase, db.Model):
    """
        description: Assignation of Gears
    """

    __tablename__ = "affectation"
    id = db.Column(db.String(255), primary_key=True)

    startDateAssignation = db.Column(db.DateTime)
    endDateAssignation = db.Column(db.DateTime)

    gearId = db.Column(db.String(255), db.ForeignKey('gear.id', ondelete='CASCADE'), nullable=False)
    acteurId = db.Column(db.String(255), db.ForeignKey('acteur.id', ondelete='CASCADE'), nullable=True)
    siteId = db.Column(db.String(255), db.ForeignKey('site.id', ondelete='CASCADE'), nullable=True)


class Acteur(SAFRSBase, db.Model):
    """
        description: Team Leader who recieve Gears
    """
    __tablename__ = "acteur"
    id = db.Column(db.String(255), primary_key=True)
    firstname = db.Column(db.String(50), nullable=False)
    lastname = db.Column(db.String(50), nullable=False)
    phoneNumber = db.Column(db.String(20))

    affectations = db.relationship('Affectation',cascade="all,delete", backref='acteur', lazy=False)


class Site(SAFRSBase, db.Model):
    """
        description: working site
    """
    __tablename__ = "site"
    id = db.Column(db.String(255), primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    description = db.Column(db.String(500), nullable=True)
    address = db.Column(db.String(255), nullable=False)
    city = db.Column(db.String(250), nullable=False)

    affectations = db.relationship('Affectation', cascade="all,delete",backref='site', lazy=False)


def start_app(app):
    api = SAFRSAPI(app, host=HOST)
    # The method_decorators will be applied to all API endpoints
    # api.expose_object(Auth, method_decorators=[auth.login_required])
    api.expose_object(User)
    api.expose_object(Category, method_decorators=[auth.login_required])
    api.expose_object(Gear, method_decorators=[auth.login_required])
    api.expose_object(Affectation, method_decorators=[auth.login_required])
    api.expose_object(Acteur, method_decorators=[auth.login_required])
    api.expose_object(Site, method_decorators=[auth.login_required])
    CORS(app)
    app.run(host=HOST, port=PORT)


#
# APP Initialization
#

app = Flask(__name__)

app.config.update(
    SQLALCHEMY_DATABASE_URI="sqlite:///db.sqlite",
    SQLALCHEMY_TRACK_MODIFICATIONS=True,
    SECRET_KEY=b"sdqfjqsdfqizroqnxw0c",
    DEBUG=True,

)

HOST = "161.97.94.5"
# HOST = "localhost"
PORT = 5000
db.init_app(app)

# Start the application
with app.app_context():
    db.create_all()
    start_app(app)
