#!/bin/bash
apt -y update
apt -y upgrade
apt -y install python3 python3-venv python3-pip apache2 libapache2-mod-wsgi-py3 python-dev libapache2-mod-wsgi python-dev
python3 -m pip install --upgrade pip
python3 -m venv venv
echo "Python virtual env created successfully"
source venv/bin/activate
echo "Python virtual env activated successfully"
python3 -m pip install -r ./requirement.txt
echo "requirement installed successfully"
kill -9 `pidof python3`
nohup python3 ./app.py &
